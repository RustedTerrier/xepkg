// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{cmp::Ordering, env, io::Write, path::Path, process::{exit, Command}};
mod repo;
mod parser;
mod pkg;

use parser::Xepkg;

const HELP_MESSAGE: &str = "Xepkg version 1.0.0
usage: xepkg {COMMAND}

DESCRIPTION
    Xepkg is a source based package manager which is primarily focused on user choice.
    The cache for packages is located in /var/cache/xepkg and the repositories are located in /etc/xepkg/repo.
    Xepkg supports custom repositories, which can be as simple as adding .xepkg files in a new directory in /etc/xepkg/repo if the user doesn't want to distribute it.

COMMANDS
    --add, add, -a, a [repo-link]
        Installs a new repo for xepkg. Repo-link must end with an archive like https://rsterrier.dev/xerux/main/LATEST.tar.xz.

    --download, download, -d, d [package1, package2, ...]
        Downloads the following packages. If there is already an archive downloaded, remove that download and download it again.

    --help, help, -h, h
        Prints this message.

    --info, info, -I, I [package]
        Lists the name, repository, description, license, status, and keywords of a package.

    --install, install, -i, i [package1, package2, ...]
        Installs the following packages.

    --list, list, -l, l [repo-name|all|installed]
        If the following argument is 'all' then list all available packages. If it's 'installed' then list all installed packages. Otherwise, assume it's a repository and list all the packages in that repository.

    --logo, logo, -L, L
        Prints out the xefetch logo for Xerux.

    --new, new, -n, n [repository]
        Creates a new package and places it in /etc/xepkg/repo/[repository]. Prompts for all the fields in a .xepkg file except rm which must be added manually.

    --remove, remove, -r, r [package1, package2, ...]
        Uninstalls the following packages if they are installed.
    
    --search, search, -s, s [search_term]
        Searches all installed repos' packages' keywords, names, and descriptions for the search term.

    --update, update, -u, u [package]
        Updates the following package if it can be updated. If no package is given, attempt to update all packages.

    --upgrade, upgrade, -U, U
        Updates all of the repositories if they can be updated.

EXIT STATUS
    On success, 0 is returned, on error, 1 is returned.

EXAMPLES
    Example 1. Installing vim and ncurses
        # xepkg -i vim ncurses
        Would you like to install libgtk-3 for vim? [Y/n]
        n
        Would you like to install libxt for vim? [Y/n]
        n
        Would you like to install python3 for vim? [Y/n]
        n
        Would you like to install valgrind for vim? [Y/n]
        n
        Would you like to install libiconv for ncurses? [Y/n]
        n
        Would you like to install libtool for ncurses? [Y/n]
        n
        Would you like to install termcap for ncurses? [Y/n]
        n
        Which dependency would you like to install: [gawk, mawk, nawk]
        gawk
        Would you like to enable multibyte for vim? [Y/n]
        n
        Would you like to enable widec for ncurses? [Y/n]
        y
        Would you like to install the following packages? [Y/n]
        vim, ncurses
        
        Downloading vim-8.2.
        Download finished.
        Installing vim-8.2.
        Found /var/cache/xepkg/ncurses-6.3/: skipping download.
        -*- Compiling Output -*-
        Successfully installed vim-8.2.
        -*- Compiling Output -*-
        Successfully installed ncurses-6.3.

        Note: in the actual output, -*- Compiling Output -*- would be replace with actual compiling output.

ENVIRONMENT
    $XEPKG_ROOT
        Changes the installation prefix. This affects the install and remove commands.

    $XEPKG_COLOR
        Changes the color of xepkg messages. By default it's green. Accepted values include: Red, Green, Yellow, Blue, Magenta, Cyan, Black, White and Bright/Light variants, such as BrightRed.";

fn main() {
    let color = get_color();
    let color_reset = "\x1b[0m";

    let mut args: Vec<String> = env::args().collect();
    if args.len() >= 2 {
        match &args[1][..] {
            | "--logo" | "logo" | "-L" | "L" => {
                println!(
                         "\
\x1b[32mVMMA    AMMV\x1b[0m
\x1b[97m VMMA  AMMV
  VMMAAMMV
   XMMMMX
  AMMVVMMA
 AMMV  VMMA
\x1b[32mAMMV    VMMA\x1b[0m"
                );
            },
            | "--new" | "new" | "-n" | "n" => {
                if args.len() >= 3 {
                    parser::Xepkg::new(&color, color_reset, &args[2]);
                } else {
                    parser::Xepkg::new(&color, color_reset, "main");
                }
            },
            | "--add" | "add" | "-a" | "a" => {
                if args.len() >= 3 {
                    parser::Repo::new(&args[2], &color, color_reset);
                } else {
                    println!("{}Usage: xepkg add LINK/TO/REPO/LATEST.TARBALL.{}", color, color_reset);
                }
            },
            | "--list" | "list" | "-l" | "l" => {
                if args.len() >= 3 {
                    match &args[2][..] {
                        | "all" => {
                            let repos = repo::list_repos(&color, color_reset);
                            for repo in repos {
                                println!("REPO: {}", &repo);
                                let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &repo)), &color, color_reset);
                                for pkg in pkgs {
                                    println!("{}", pkg);
                                }
                            }
                        },
                        | "installed" => {
                            let repos = repo::list_repos(&color, color_reset);
                            for repo in repos {
                                let pkg_list =
                                    repo::list_installed_pkgs(&repo,
                                                              &color,
                                                              color_reset);
                                print!("{}", pkg_list);
                            }
                        },
                        | a => {
                            let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &a)), &color, color_reset);
                            for pkg in pkgs {
                                println!("{}", pkg);
                            }
                        }
                    }
                } else {
                    println!("{}Usage: xepkg list [all|installed|REPO_NAME]{}",
                             color, color_reset);
                }
            },
            | "--remove" | "remove" | "-r" | "r" => {
                if args.len() >= 3 {
                    let repos = repo::list_repos(&color, color_reset);
                    // remove the command and sub-command
                    args.remove(0);
                    args.remove(0);
                    for arg in args {
                        for repo in &repos {
                            let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &repo)), &color, color_reset);
                            for pkg in pkgs {
                                if pkg == arg {
                                    let pkg = parser::parse(&format!("/etc/xepkg/repo/{}/{}.xepkg", &repo, &pkg), &color, color_reset);
                                    if pkg.is_installed(&color, color_reset) {
                                        pkg.uninstall(&color, "\x1b[0m");
                                    } else {
                                        println!("{}Package {} is not installed.{}", color, pkg.name, color_reset);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                } else {
                    println!("{}Usage: xepkg remove package1 package2 ...{}",
                             color, color_reset);
                }
            },
            | "--install" | "install" | "-i" | "i" => {
                if args.len() >= 3 {
                    let repos = repo::list_repos(&color, color_reset);
                    // remove the command and sub-command
                    args.remove(0);
                    args.remove(0);
                    let mut pkgs_vec = Vec::new();
                    for arg in args {
                        for repo in &repos {
                            let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &repo)), &color, color_reset);
                            for pkg in pkgs {
                                if pkg == arg {
                                    pkgs_vec.push(parser::parse(&format!("/etc/xepkg/repo/{}/{}.xepkg", &repo, &pkg), &color, color_reset));
                                    break;
                                }
                            }
                        }
                    }
                    pkg::install(&pkgs_vec, &color, color_reset, false);
                } else {
                    println!("{}Usage: xepkg install package1 package2 ...{}",
                             color, color_reset);
                }
            },
            | "--info" | "info" | "-I" | "I" => {
                if args.len() >= 3 {
                    let repos = repo::list_repos(&color, color_reset);
                    for i in repos {
                        let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &i)), &color, color_reset);
                        for j in pkgs {
                            if j == args[2] {
                                let pkg_info = parser::parse(&format!("/etc/xepkg/repo/{}/{}.xepkg", &i, &j), &color, color_reset);
                                print!(
                                       "\
Name: {}
Repo: {}
Description: {}
License: {}
Status: {}
Keywords: ",
                                       &pkg_info.name,
                                       &pkg_info.repository,
                                       &pkg_info.description,
                                       &pkg_info.license,
                                       &pkg_info.status
                                );
                                for k in 0 .. pkg_info.keywords.len() {
                                    if k != pkg_info.keywords.len() - 1 {
                                        print!("{}, ", pkg_info.keywords[k]);
                                    } else {
                                        print!("{}", pkg_info.keywords[k]);
                                    }
                                }
                                println!();
                            }
                        }
                    }
                } else {
                    println!("{}Usage: xepkg info package{}",
                             color, color_reset);
                }
            },
            | "--download" | "download" | "-d" | "d" => {
                if args.len() >= 3 {
                    let repos = repo::list_repos(&color, color_reset);
                    args.remove(0);
                    args.remove(0);
                    let mut pkgs_vec = Vec::new();
                    for i in args {
                        for j in &repos {
                            let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &j)), &color, color_reset);
                            for k in pkgs {
                                if k == i {
                                    pkgs_vec.push(parser::parse(&format!("/etc/xepkg/repo/{}/{}.xepkg", &j, &k), &color, color_reset));
                                    break;
                                }
                            }
                        }
                    }
                    for i in pkgs_vec {
                        i.download(&color, color_reset);
                    }
                } else {
                    println!("{}Usage: xepkg download package1 package2 ...{}",
                             color, color_reset);
                }
            },
            | "--update" | "update" | "-u" | "u" => {
                if args.len() >= 3 {
                    let repos = repo::list_repos(&color, color_reset);
                    for i in repos {
                        let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &i)), &color, color_reset);
                        for j in pkgs {
                            if j == args[2] {
                                let pkg = parser::parse(&format!("/etc/xepkg/repo/{}/{}.xepkg", &i, &j), &color, color_reset);
                                match pkg.can_update(&color, color_reset) {
                                    | Ok(false) => println!("{}Package {} is already up to date.{}", color, pkg.name, color_reset),
                                    | Err(e) => {
                                        eprintln!("{}ERROR: cannot update {}: {}{}", color, pkg.name, e, color_reset);
                                        exit(1);
                                    },
                                    | Ok(true) => {
                                        pkg.uninstall(&color, color_reset);
                                        pkg::install(&[pkg], &color, color_reset, false);
                                    }
                                }
                                break;
                            }
                        }
                    }
                } else {
                    let mut pkgs_to_update = Vec::new();

                    let mut installed_pkgs_string = String::new();
                    let repos = repo::list_repos(&color, color_reset);
                    for i in repos {
                        let pkg_list =
                            repo::list_installed_pkgs(&i, &color, color_reset);
                        installed_pkgs_string.push_str(&pkg_list);
                    }
                    let mut installed_pkgs: Vec<&str> =
                        installed_pkgs_string.split('\n').collect();
                    installed_pkgs.pop();
                    for i in installed_pkgs {
                        // remove the version from the line
                        let mut pkg_name_vec: Vec<&str> =
                            i.split('-').collect();
                        pkg_name_vec.remove(pkg_name_vec.len() - 1);
                        let pkg_name = pkg_name_vec.join("-");
                        match pkg::search_for_package(&pkg_name,
                                                      &color,
                                                      color_reset)
                        {
                            | Ok(a) => {
                                match a.can_update(&color, color_reset) {
                                    | Err(e) => {
                                        eprintln!("{}ERROR: cannot update {}: {}{}", color, pkg_name, e, color_reset);
                                        exit(1);
                                    },
                                    | Ok(true) => {
                                        pkgs_to_update.push(a.to_owned());
                                    },
                                    | _ => ()
                                }
                            },
                            | Err(_) => {
                                eprintln!("{}ERROR: cannot find xepkg file for {}{}", color, pkg_name, color_reset);
                                exit(1);
                            }
                        }
                    }
                    if !pkgs_to_update.is_empty() {
                        println!("Would you like to update the following packages? [Y/n]");
                        for (i, j) in pkgs_to_update.iter().enumerate() {
                            if i != 0 {
                                print!(", {}", j.name);
                            } else {
                                print!("{}", j.name);
                            }
                        }
                        println!();

                        let response =
                            read_line(&color, color_reset).to_ascii_uppercase();

                        if response != "N" && response != "NO" {
                            for i in &pkgs_to_update {
                                i.uninstall(&color, color_reset);
                            }
                            // Set yes to not reprompt
                            pkg::install(&pkgs_to_update,
                                         &color,
                                         color_reset,
                                         true);
                        }
                    }
                }
            },
            | "--upgrade" | "upgrade" | "-U" | "U" => {
                let repos = repo::list_repos(&color, color_reset);
                for i in repos {
                    parser::parse_repo(&format!("/etc/xepkg/repo/{}", i),
                                       &color,
                                       color_reset).try_update(&color,
                                                               color_reset);
                }
            },
            | "--search" | "search" | "-s" | "s" => {
                if args.len() >= 3 {
                    let repos = repo::list_repos(&color, color_reset);
                    for i in repos {
                        let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &i)), &color, color_reset);
                        let mut found_pkgs = Vec::new();
                        for j in pkgs {
                            let pkg = parser::parse(&format!("/etc/xepkg/repo/{}/{}.xepkg", &i, &j), &color, color_reset);
                            if pkg.name.contains(&args[2])
                               || pkg.description.contains(&args[2])
                               || pkg.keywords.contains(&args[2])
                            {
                                let inst = match pkg.is_installed(&color,
                                                                  color_reset)
                                {
                                    | true => "[*] ",
                                    | false => "[ ] "
                                };
                                found_pkgs.push((format!("{}{}-{}",
                                                         inst,
                                                         pkg.name,
                                                         pkg.version),
                                                 pkg.description));
                            }
                        }
                        let mut biggest_name: usize = 0;
                        for j in &found_pkgs {
                            if j.0.len() > biggest_name {
                                biggest_name = j.0.len();
                            }
                        }
                        biggest_name += 1;
                        for j in &found_pkgs {
                            println!("{}{}{}",
                                     j.0,
                                     " ".repeat(biggest_name - j.0.len()),
                                     j.1);
                        }
                    }
                } else {
                    println!("{}Usage: xepkg search SEARCH_STRING{}",
                             color, color_reset);
                }
            },
            | "--help" | "help" | "-h" | "h" => println!("{}", HELP_MESSAGE),
            | _ => ()
        }
    } else {
        println!("{}", HELP_MESSAGE);
    }
}

fn get_color() -> String {
    let env_var = match env::var("XEPKG_COLOR") {
        | Ok(a) => {
            if a.starts_with("\\x1b[") {
                a.replace("\\x1b", "\x1b")
            } else if a.starts_with("Light") | a.starts_with("Bright") {
                match a.trim_start_matches("Light").trim_start_matches("Bright")
                {
                    | "Red" => "\x1b[91m".to_string(),
                    | "Green" => "\x1b[92m".to_string(),
                    | "Yellow" => "\x1b[93m".to_string(),
                    | "Blue" => "\x1b[94m".to_string(),
                    | "Magenta" => "\x1b[95m".to_string(),
                    | "Cyan" => "\x1b[96m".to_string(),
                    | _ => "\x1b[32m".to_string()
                }
            } else {
                match &a[..] {
                    | "Red" => "\x1b[31m".to_string(),
                    | "Green" => "\x1b[32m".to_string(),
                    | "Yellow" => "\x1b[33m".to_string(),
                    | "Blue" => "\x1b[34m".to_string(),
                    | "Magenta" => "\x1b[35m".to_string(),
                    | "Cyan" => "\x1b[36m".to_string(),
                    | _ => "\x1b[32m".to_string()
                }
            }
        },
        | Err(_) => "\x1b[32m".to_string()
    };
    env_var
}

pub fn vec_str_to_vec_string(vec: Vec<&str>) -> Vec<String> {
    let mut vec_string = Vec::new();
    for i in vec {
        vec_string.push(i.to_string());
    }
    vec_string
}

pub fn read_line(color: &str, color_reset: &str) -> String {
    let mut line = String::new();
    match std::io::stdout().flush() {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("{}ERROR: Unable to flush output: {}.{}",
                      color, e, color_reset);
            exit(1);
        }
    }
    match std::io::stdin().read_line(&mut line) {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("{}ERROR: Unable to read line: {}.{}",
                      color, e, color_reset);
            exit(1);
        }
    };
    line.replace('\n', "")
}

// returns true if the current version is newer
pub fn is_newer(current: &[u8], installed: &[u8]) -> bool {
    if current.len() != installed.len() {
        false
    } else {
        let mut output: bool = false;
        for i in 0 .. current.len() {
            match current[i].cmp(&installed[i]) {
                | Ordering::Greater => {
                    output = true;
                    break;
                },
                | Ordering::Less => {
                    output = false;
                    break;
                },
                | _ => ()
            }
        }
        output
    }
}

fn extract(file_extension: &str, pkg: &Xepkg, color: &str, color_reset: &str) {
    if file_extension.contains(".tar") {
        let mut child =
            match Command::new("tar").arg("xaf")
                                     .arg(format!("/var/cache/xepkg/{}",
                                                  file_extension))
                                     .arg("-C")
                                     .arg(format!("/var/cache/xepkg/{}-{}-tmp",
                                                  &pkg.name, &pkg.version))
                                     .spawn()
            {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: unable to launch command `tar xaf /var/cache/xepkg/{} -C /var/cache/xepkg/{}-{}-tmp`: {}{}", color, file_extension, &pkg.name, &pkg.version, e, color_reset);
                    exit(1);
                }
            };
        // Assume it worked for now
        match child.wait() {
            | Ok(_) => (),
            | Err(e) => {
                eprintln!("{}ERROR: unable to wait for command `tar xaf /var/cache/xepkg/{} -C /var/cache/xepkg/{}-{}-tmp` to finish: {}{}",
                          color, file_extension, &pkg.name, &pkg.version, e, color_reset);
                exit(1);
            }
        };
    }
}
