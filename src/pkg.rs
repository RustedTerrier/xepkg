// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{env, fs, io::Write, path::Path, process::{exit, Command, Stdio}, thread};

use crate::{extract, parser, parser::Xepkg, read_line, repo, vec_str_to_vec_string};

pub fn install(pkgs_to_install: &[Xepkg], color: &str, color_reset: &str,
               yes: bool) {
    // Grab all the dependencies that need to be installed
    let mut pkg_install_list = Vec::new();
    for i in pkgs_to_install {
        if !i.is_installed(color, color_reset) {
            for j in get_deps(i.to_owned(), color, color_reset, &[]) {
                pkg_install_list.push(j);
            }
        }
    }

    for i in pkg_install_list.iter_mut() {
        *i = (i.0.to_owned(), format!("{} {}", i.1, i.0.to_owned().get_optional_dep_args(color, color_reset)).trim_start_matches(' ').trim_end_matches(' ').to_string());
    }

    let response: String = if !yes {
        println!("{}Would you like to install the following packages? [Y/n]{}",
                 color, color_reset);
        for (i, _) in pkg_install_list.iter().enumerate() {
            if i != 0 {
                print!(", {}", pkg_install_list[i].0.name);
            } else {
                print!("{}", pkg_install_list[i].0.name);
            }
        }
        println!();

        read_line(color, color_reset).to_ascii_uppercase()
    } else {
        "YES".to_string()
    };

    if response == "N" || response == "NO" {
        println!("{}Aborting installation.{}", color, color_reset);
    } else {
        // Create thread versions of variables so that they aren't used elsewhere and ownership
        // becomes simplified.
        let thread_color = color.to_owned();
        let thread_color_reset = color_reset.to_owned();
        let thread_pkg_install_list = pkg_install_list.clone();
        thread::spawn(move || {
            for (pkg, _) in thread_pkg_install_list {
                let cache_path = &format!("/var/cache/xepkg/{}-{}/",
                                          &pkg.name, &pkg.version);
                let cache_path = Path::new(&cache_path);

                println!("{}Downloading {}{}-{}{}.{}",
                         &thread_color,
                         &thread_color_reset,
                         &pkg.name,
                         &pkg.version,
                         &thread_color,
                         &thread_color_reset);

                if cache_path.exists() {
                    println!("{}Found {}/var/cache/xepkg/{}-{}/{}: skipping download.{}", &thread_color, &thread_color_reset, &pkg.name, &pkg.version, &thread_color, &thread_color_reset);
                } else {
                    pkg.download(&thread_color, &thread_color_reset);
                }
            }
        });

        // Install each package with it's configured features
        for (pkg, feat) in pkg_install_list {
            loop {
                let cache_path = &format!("/var/cache/xepkg/{}-{}/",
                                          &pkg.name, &pkg.version);
                let cache_path = Path::new(&cache_path);

                // Start installing it once it's done downloading
                if cache_path.exists() {
                    println!("{}Installing {}{}-{}{}.{}",
                             color,
                             color_reset,
                             &pkg.name,
                             &pkg.version,
                             color,
                             color_reset);

                    match env::set_current_dir(&cache_path) {
                        | Ok(a) => a,
                        | Err(e) => {
                            eprintln!("{}ERROR: unable to set current directory to {}{}{}: {}.{}", color, color_reset, &cache_path.display(), color, e, color_reset);
                            exit(1);
                        }
                    }

                    // If there is only 1 directory in the directory cd into it
                    match fs::read_dir(&cache_path) {
                        | Ok(a) => {
                            let entries: Vec<Result<fs::DirEntry,
                                                    std::io::Error>> =
                                a.collect();
                            if entries.len() == 1 {
                                let file_type = match &entries[0] {
                                    | Ok(a) => match a.file_type() {
                                        | Ok(a) => a.is_dir(),
                                        // Abort changing PWD
                                        | Err(_) => false
                                    },
                                    // Abort changing PWD
                                    | Err(_) => false
                                };
                                if file_type {
                                    let mut path = cache_path.to_path_buf();
                                    path.push(match entries[0].as_ref() {
                                                  | Ok(a) => a.path(),
                                                  | Err(_) => unreachable!()
                                              });
                                    match env::set_current_dir(path.as_path()) {
                                        | Ok(a) => a,
                                        | Err(e) => {
                                            eprintln!("{}ERROR: unable to set current directory to {}{}{}: {}{}", color, color_reset, cache_path.display(), color, e, color_reset);
                                            exit(1);
                                        }
                                    }
                                }
                            }
                        },
                        // Abort changing PWD
                        | Err(_) => ()
                    }

                    // Check if XEPKG_ROOT exists and change the prefix to that if so. Otherwise
                    // default to /usr
                    let root = match env::var("XEPKG_ROOT") {
                        | Ok(a) => match a.is_empty() {
                            | true => "/usr".to_string(),
                            | false => a
                        },
                        | Err(_) => "/usr".to_string()
                    };

                    // Build the package by executing posix shell to run the build command
                    let mut child =
                        match Command::new("sh").arg("-c")
                                                .arg(&pkg.build_cmd
                                                         .replace("{ROOT}",
                                                                  &root)
                                                         .replace("{ARGS}",
                                                                  &feat))
                                                .stdout(Stdio::inherit())
                                                .spawn()
                        {
                            | Ok(a) => a,
                            | Err(e) => {
                                eprintln!("{}ERROR: unable to build {}: unable to run `sh -c {}`: {}{}", color, &pkg.name, &pkg.build_cmd, e, color_reset);
                                exit(1);
                            }
                        };
                    // Check whether it was successful; if so, mark the package as installed.
                    match child.wait() {
                        | Ok(a) => match a.code() {
                            | Some(0) => {
                                pkg.mark_installed(color, color_reset);
                                println!("{}Successfully installed {}{}-{}{}.{}", color, color_reset, &pkg.name, &pkg.version, color, color_reset);
                            },
                            | Some(e) => {
                                eprintln!("{}ERROR: {} failed to install: {}.{}", color, &pkg.name, e, color_reset);
                                exit(1);
                            },
                            | None => {
                                eprintln!("{}ERROR: {} failed to install.{}",
                                          color, &pkg.name, color_reset);
                                exit(1);
                            }
                        },
                        | Err(e) => {
                            eprintln!("{}ERROR: {} failed to install: {}.{}",
                                      color, &pkg.name, e, color_reset);
                            exit(1);
                        }
                    };
                    break;
                }
            }
        }
    }
}

fn get_deps(mut pkg: Xepkg, color: &str, color_reset: &str,
            existing_pkgs: &[(Xepkg, String)])
            -> Vec<(Xepkg, String)> {
    // Grab the optional dependencies
    let mut optional_args = String::new();
    for (opt_name, arg) in &pkg.optional_deps {
        println!("{}Would you like to install {} for {}? [Y/n]{}",
                 color, opt_name, pkg.name, color_reset);
        let response = read_line(color, color_reset).to_ascii_uppercase();

        if response != "N" && response != "NO" {
            pkg.deps.push(opt_name.to_owned());
            optional_args.push_str(arg);
            optional_args.push(' ');
        }
    }

    // The first part is the package data and the second is the optional arguments for configure
    let mut pkg_repos: Vec<(Xepkg, String)> = Vec::new();
    for i in pkg.deps.iter_mut() {
        // | is an OR for packages: ex: (pkg1|pkg2) is prompt for pkg1 or pkg2
        if i.contains('|') {
            let dep: Vec<String> =
                vec_str_to_vec_string(i.split('|').collect());
            let mut response: String = String::new();
            while !dep.contains(&response) {
                print!("{}Which dependency would you like to install? [",
                       color);
                for (i, j) in dep.iter().enumerate() {
                    if i != 0 {
                        print!(", {}", j)
                    } else {
                        print!("{}", j)
                    }
                }
                println!("]{}", color_reset);
                response = read_line(color, color_reset);
            }
            *i = response.to_string();
        }

        match search_for_package(i, color, color_reset) {
            // Find deps as well and push each
            | Ok(a) => {
                // Only check for dependencies if it isn't installed
                if !a.is_installed(color, color_reset) {
                    // TODO: this is super messy and slow, so fix it later.
                    let mut full_list = pkg_repos.clone();
                    for j in existing_pkgs {
                        full_list.push((j.0.to_owned(), j.1.to_owned()));
                    }

                    for (dep, arg) in
                        get_deps(a.to_owned(), color, color_reset, &full_list)
                    {
                        let mut found = false;
                        for (next_pkg, _) in &full_list {
                            if next_pkg == &dep {
                                found = true;
                                break;
                            }
                        }
                        if !found {
                            pkg_repos.push((dep, arg));
                        }
                    }
                }
            },
            | Err(_) => {
                // TODO: Look for the dependency in PATH and LIBPATH
                eprintln!("{}Unable to install {}{}{}.{}",
                          color, color_reset, pkg.name, color, color_reset);
                exit(1);
            }
        }
    }
    pkg_repos.push((pkg, optional_args));
    pkg_repos
}

pub fn search_for_package(pkg_name: &str, color: &str, color_reset: &str)
                          -> Result<Xepkg, ()> {
    // The first way I thought of responding if a package isn't found.
    let mut found_pkg = String::new();
    let repos = repo::list_repos(color, color_reset);
    for i in &repos {
        let pkgs =
            repo::list_bundles(Path::new(&format!("/etc/xepkg/repo/{}", &i)),
                               color,
                               color_reset);
        for j in pkgs {
            if j == pkg_name && found_pkg.is_empty() {
                found_pkg = format!("/etc/xepkg/repo/{}/{}.bundle", &i, &j);
                break;
            }
        }
    }
    for i in &repos {
        let pkgs = repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}",
                                                      &i)),
                                   color,
                                   color_reset);
        for j in pkgs {
            if j == pkg_name && found_pkg.is_empty() {
                found_pkg = format!("/etc/xepkg/repo/{}/{}.xepkg", &i, &j);
                break;
            }
        }
    }
    if found_pkg.is_empty() {
        println!("{}Unable to find package {}{}{}.{}",
                 color, color_reset, pkg_name, color, color_reset);
        Err(())
    } else if found_pkg.ends_with(".xepkg") {
        // Just parse the file and return the xepkg file
        Ok(parser::parse(&found_pkg, color, color_reset))
    } else {
        // For bundles I can just output the full file since it should just be something like
        // `gawk|mawk|nawk`
        let bundle = match fs::read_to_string(&found_pkg) {
            | Ok(a) => a.replace('\n', ""),
            | Err(e) => {
                eprintln!("{}Invalid XEPKG setup: unable to read {}{}{}: {}.",
                          color, color_reset, found_pkg, color, e);
                exit(1);
            }
        };
        // Split it up into the different options and break once the input matches an option
        let dep: Vec<String> =
            vec_str_to_vec_string(bundle.split('|').collect());
        let mut response: String = String::new();
        while !dep.contains(&response) {
            print!("{}Which dependency would you like to install: [", color);
            for (i, j) in dep.iter().enumerate() {
                if i != 0 {
                    print!(", {}", j)
                } else {
                    print!("{}", j)
                }
            }
            println!("]{}", color_reset);
            response = read_line(color, color_reset);
        }
        search_for_package(&response, color, color_reset)
    }
}

impl Xepkg {
    pub fn to_owned(&self) -> Xepkg {
        Xepkg { name:              self.name.to_string(),
                repository:        self.repository.to_string(),
                version:           self.version.to_string(),
                description:       self.description.to_string(),
                license:           self.license.to_string(),
                status:            self.status.to_string(),
                keywords:          self.keywords.clone(),
                deps:              self.deps.clone(),
                optional_deps:     self.optional_deps.clone(),
                optional_features: self.optional_features.clone(),
                build_cmd:         self.build_cmd.to_string(),
                rm_cmd:            self.rm_cmd.to_string() }
    }

    pub fn is_installed(&self, color: &str, color_reset: &str) -> bool {
        let mut installed = false;

        let repos = repo::list_repos(color, color_reset);
        for i in repos {
            let pkg_list = repo::list_installed_pkgs(&i, color, color_reset);
            let pkg_list_vec: Vec<&str> = pkg_list.split('\n').collect();
            for j in pkg_list_vec {
                if j.starts_with(&format!("{}-", self.name)) {
                    installed = true;
                    break;
                }
            }
        }
        installed
    }

    pub fn mark_installed(&self, color: &str, color_reset: &str) {
        // The first way I thought of responding if a package isn't found.
        let mut found_repo = String::new();
        let repos = repo::list_repos(color, color_reset);
        for i in repos {
            let pkgs =
                repo::list_pkgs(Path::new(&format!("/etc/xepkg/repo/{}", &i)),
                                color,
                                color_reset);
            for j in pkgs {
                if j == self.name && found_repo.is_empty() {
                    found_repo = format!("/etc/xepkg/repo/{}", &i);
                    break;
                }
            }
        }
        if !found_repo.is_empty() {
            // Has already been read by now, it should not be able to fail.
            let repo_path = &format!("{}/installed", found_repo);
            let mut installed_pkgs = fs::read_to_string(repo_path).unwrap();
            installed_pkgs =
                format!("{}{}-{}\n", installed_pkgs, &self.name, &self.version);
            match fs::write(&repo_path, &installed_pkgs) {
                | Err(e) => {
                    eprintln!("{}Unable to write to {}: {}.{}",
                              color, repo_path, e, color_reset);
                    exit(1);
                },
                | Ok(_) => ()
            };
        }
    }

    pub fn mark_uninstalled(&self, color: &str, color_reset: &str) {
        let repos = repo::list_repos(color, color_reset);
        for i in repos {
            let pkg_list = repo::list_installed_pkgs(&i, color, color_reset);
            let mut pkg_list_vec: Vec<&str> = pkg_list.split('\n').collect();
            for (j, k) in pkg_list_vec.iter().enumerate() {
                if k.starts_with(&format!("{}-", self.name)) {
                    pkg_list_vec.remove(j);
                    break;
                }
            }
            match fs::write(format!("/etc/xepkg/repo/{}/installed", i),
                            pkg_list_vec.join("\n"))
            {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: unable to write to /etc/xepkg/repo/{}/installed: {}{}", color, i, e, color_reset);
                    exit(1);
                }
            }
        }
    }

    pub fn uninstall(&self, color: &str, color_reset: &str) {
        let path: String =
            format!("/var/cache/xepkg/{}-{}", self.name, self.version);
        let path = Path::new(&path);
        if Path::new(&path).exists() {
            match env::set_current_dir(&Path::new(&path)) {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: unable to set current directory: {}{}",
                              color, e, color_reset);
                    exit(1);
                }
            }
        }

        // If there is only 1 directory in the directory cd into it
        match fs::read_dir(&path) {
            | Ok(a) => {
                let entries: Vec<Result<fs::DirEntry, std::io::Error>> =
                    a.collect();
                if entries.len() == 1 {
                    let file_type = match &entries[0] {
                        | Ok(a) => match a.file_type() {
                            | Ok(a) => a.is_dir(),
                            // Just abort changing PWD
                            | Err(_) => false
                        },
                        // Just abort changing PWD
                        | Err(_) => false
                    };
                    if file_type {
                        let mut path = path.to_path_buf();
                        path.push(match entries[0].as_ref() {
                                      | Ok(a) => a.path(),
                                      | Err(_) => unreachable!()
                                  });
                        match env::set_current_dir(path.as_path()) {
                            | Ok(a) => a,
                            | Err(e) => {
                                eprintln!("{}ERROR: unable to set current directory: {}{}", color, e, color_reset);
                                exit(1);
                            }
                        }
                    }
                }
            },
            // Just abort changing PWD
            | Err(_) => ()
        }

        let root = match env::var("XEPKG_ROOT") {
            | Ok(a) => match a.is_empty() {
                | true => "/usr".to_string(),
                | false => a
            },
            | Err(_) => "/usr".to_string()
        };
        let mut child =
            match Command::new("sh").arg("-c")
                                    .arg(&self.rm_cmd.replace("{ROOT}", &root))
                                    .stdout(Stdio::inherit())
                                    .spawn()
            {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: unable to build {}: unable to run `sh -c {}`: {}{}", color, &self.name, &self.build_cmd, e, color_reset);
                    exit(1);
                }
            };

        match child.wait() {
            | Ok(a) => match a.code() {
                | Some(0) => {
                    self.mark_uninstalled(color, color_reset);
                    println!("{}Successfully removed {}{}-{}{}.{}",
                             color,
                             color_reset,
                             &self.name,
                             &self.version,
                             color,
                             color_reset);
                },
                | Some(e) => {
                    eprintln!("{}ERROR{}: {} failed to install: {}",
                              color, color_reset, &self.name, e);
                    exit(1);
                },
                | None => {
                    eprintln!("{}ERROR{}: {} failed to install",
                              color, color_reset, &self.name);
                    exit(1);
                }
            },
            | Err(e) => {
                eprintln!("{}ERROR{}: {} failed to install: {}",
                          color, color_reset, &self.name, e);
                exit(1);
            }
        };
    }

    pub fn new(color: &str, color_reset: &str, repo: &str) -> Xepkg {
        print!("Name: ");
        let name = format!("[{}]", read_line(color, color_reset));
        print!("Repository: ");
        let repository = format!("repo: {}", read_line(color, color_reset));
        print!("Verison: ");
        let version = format!("version: {}", read_line(color, color_reset));
        print!("Description: ");
        let description =
            format!("description: {}", read_line(color, color_reset));
        print!("License: ");
        let license = format!("license: {}", read_line(color, color_reset));
        print!("Status: ");
        let status = format!("status: {}", read_line(color, color_reset));
        print!("Keywords: ");
        let keywords = format!("keywords: {}", read_line(color, color_reset));
        print!("Deps: ");
        let deps = format!("deps: {}", read_line(color, color_reset));
        print!("Optional-Deps: ");
        let optional_deps =
            format!("optional-deps: {}", read_line(color, color_reset));
        print!("Optional-Features: ");
        let optional_features =
            format!("optional-feat: {}", read_line(color, color_reset));
        print!("Build: ");
        let build_cmd = format!("build: {}", read_line(color, color_reset));
        let file = format!("{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n{}\n",
                           name,
                           repository,
                           version,
                           description,
                           license,
                           status,
                           keywords,
                           deps,
                           optional_deps,
                           optional_features,
                           build_cmd);

        match fs::write(format!("/etc/xepkg/repo/{}/{}.xepkg",
                                repo,
                                name.replace('[', "").replace(']', "")),
                        file)
        {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: unable to write to {}/etc/xepkg/repo/default/{}{}: {}.{}", color, color_reset, name.replace('[', "").replace(']', ""), color, e, color_reset);
                exit(1);
            }
        }
        parser::parse(&format!("/etc/xepkg/repo/{}/{}.xepkg",
                               repo,
                               name.replace('[', "").replace(']', "")),
                      color,
                      color_reset)
    }

    // Returns Ok(true) if it updated the package, Ok(false) if it was up to date, and Err() if it
    // couldn't update.
    pub fn can_update(&self, color: &str, color_reset: &str)
                      -> Result<bool, String> {
        // Find the installed version
        let mut installed_version: Vec<u8> = Vec::new();
        let repos = repo::list_repos(color, color_reset);
        for i in repos {
            let pkg_list = repo::list_installed_pkgs(&i, color, color_reset);
            let pkg_list_vec: Vec<&str> = pkg_list.split('\n').collect();
            for j in pkg_list_vec {
                if j.starts_with(&format!("{}-", self.name)) {
                    // Split the version from 1.2.3 into [1, 2, 3]
                    let version = j.trim_start_matches(&format!("{}-",
                                                                self.name))
                                   .replace('_', "");
                    let version_vector_string: Vec<&str> =
                        version.split('.').collect();
                    installed_version = Vec::new();
                    for k in version_vector_string {
                        let num = k.parse::<u8>().unwrap_or(0);
                        installed_version.push(num);
                    }
                    break;
                }
            }
            if !installed_version.is_empty() {
                break;
            }
        }

        // Split the version from 1.2.3 into [1, 2, 3]
        let version_replaced = self.version.replace('_', "");
        let version_vector_string: Vec<&str> =
            version_replaced.split('.').collect();
        let mut current_version = Vec::new();
        for i in version_vector_string {
            let num = i.parse::<u8>().unwrap_or(0);
            current_version.push(num);
        }

        // Compare the current version with the installed version and update if true
        if crate::is_newer(&current_version, &installed_version) {
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub fn download(&self, color: &str, color_reset: &str) {
        let file_extension: Vec<&str> = self.repository.split('/').collect();
        let file_extension = match file_extension.last() {
            | Some(a) => a.to_string(),
            | None => String::new()
        };

        let file_content = match reqwest::blocking::get(&self.repository) {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: unable to retrieve file from {}: {}{}",
                          color, self.repository, e, color_reset);
                exit(1);
            }
        };

        let file_content = &match file_content.bytes() {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: unable to get bytes from file: {}: {}{}",
                          color, file_extension, e, color_reset);
                exit(1);
            }
        };

        let mut file =
            match fs::File::create(Path::new(&format!("/var/cache/xepkg/{}",
                                                      file_extension)))
            {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: unable to create /var/cache/xepkg/{}: {}{}", color, file_extension, e, color_reset);
                    exit(1);
                }
            };

        match file.write_all(file_content) {
            | Ok(_) => (),
            | Err(e) => {
                eprintln!("{}ERROR: unable to write to /var/xepkg/cache/{}: {}{}", color, file_extension, e, color_reset);
                exit(1);
            }
        }

        if Path::new(&format!("/var/cache/xepkg/{}-{}",
                              &self.name, &self.version)).exists()
        {
            println!("{}Found {}/var/cache/xepkg/{}-{}/{}: removing directory.{}", color, color_reset, &self.name, &self.version, color, color_reset);
            match fs::remove_dir_all(format!("/var/cache/xepkg/{}-{}",
                                             &self.name, &self.version))
            {
                | Ok(_) => (),
                | Err(e) => {
                    eprintln!("{}ERROR: cannot remove {}/var/cache/xepkg/{}-{}/{}: {}.{}", color, color_reset, &self.name, &self.version, color, e, color_reset);
                    exit(1);
                }
            }
        }

        match fs::create_dir(format!("/var/cache/xepkg/{}-{}-tmp",
                                     &self.name, &self.version))
        {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: unable to create {}/var/cache/xepkg/{}-{}/{}: {}{}", color, color_reset, &self.name, &self.version, color, e, color_reset);
                exit(1);
            }
        }

        extract(&file_extension, self, color, color_reset);

        match fs::rename(format!("/var/cache/xepkg/{}-{}-tmp",
                                 &self.name, &self.version),
                         format!("/var/cache/xepkg/{}-{}",
                                 &self.name, &self.version))
        {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: unable to rename {}/var/cache/xepkg/{}-{}-tmp/{} to {}/var/cache/xepkg/{}-{}/{}: {}{}",
                          color, color_reset, &self.name, &self.version, color, color_reset, &self.name, &self.version, color, e, color_reset);
                exit(1);
            }
        }

        println!("\r{}Download finished.{}", color, color_reset);
    }

    pub fn get_optional_dep_args(self, color: &str, color_reset: &str)
                                 -> String {
        let mut optional_args: String = String::new();
        for i in &self.optional_features {
            println!("{}Would you like to {} for {}? [Y/n]{}",
                     color, i.0, self.name, color_reset);

            let response = read_line(color, color_reset).to_ascii_uppercase();

            if response != "N" && response != "NO" {
                optional_args.push_str(&i.1);
                optional_args.push(' ');
            }
        }
        optional_args
    }
}
