// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{fs, io::Write, path::Path, process::{exit, Command}};

use crate::{is_newer, parser, parser::Repo};

pub fn list_repos(color: &str, color_reset: &str) -> Vec<String> {
    let mut repos = Vec::new();
    let path = Path::new("/etc/xepkg/repo/");
    let read_dir = match fs::read_dir(path) {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("{}Invalid XEPKG setup: Unable to read {}/etc/xepkg/repo/{}: {}.{}", color, color_reset, color, e, color_reset);
            exit(1);
        }
    };
    for entry in read_dir {
        let entry = match entry {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}Invalid XEPKG setup: Unable to read an entry in {}/etc/xepkg/repo/{}: {}.{}", color, color_reset, color, e, color_reset);
                exit(1);
            }
        };
        if entry.path().as_path().is_dir() {
            let file_name = match entry.file_name().into_string() {
                | Ok(a) => a,
                | Err(_) => {
                    eprintln!("{}Invalid XEPKG setup: Unable to read file name in {}/etc/xepkg/repo/{}: Invalid UTF8.{}", color, color_reset, color, color_reset);
                    exit(1);
                }
            };
            repos.push(file_name);
        }
    }
    repos
}

pub fn list_pkgs(path: &Path, color: &str, color_reset: &str) -> Vec<String> {
    let mut list = Vec::new();
    let read_dir = match fs::read_dir(path) {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("{}Invalid XEPKG setup: Unable to read {}{}{}: {}.{}",
                      color,
                      color_reset,
                      path.display(),
                      color,
                      e,
                      color_reset);
            exit(1);
        }
    };
    for entry in read_dir {
        let entry = match entry {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}Invalid XEPKG setup: Unable to read an entry in {}{}{}: {}.{}", color, color_reset, path.display(), color, e, color_reset);
                exit(1);
            }
        };
        let file_name = match entry.file_name().into_string() {
            | Ok(a) => a,
            | Err(_) => {
                eprintln!("{}Invalid XEPKG setup: Unable to read file name in {}{}{}: Invalid UTF8.{}", color, color_reset, path.display(), color, color_reset);
                exit(1);
            }
        };
        if file_name.ends_with(".xepkg") {
            list.push(file_name.trim_end_matches(".xepkg").to_string());
        }
    }
    list
}

pub fn list_installed_pkgs(repo: &str, color: &str, color_reset: &str)
                           -> String {
    match fs::read_to_string(&format!("/etc/xepkg/repo/{}/installed", repo)) {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("{}Invalid XEPKG setup: Unable to read {}/etc/xepkg/repo/{}/installed{}: {}.{}", color, color_reset, repo, color, e, color_reset);
            exit(1);
        }
    }
}

pub fn list_bundles(path: &Path, color: &str, color_reset: &str)
                    -> Vec<String> {
    let mut list = Vec::new();
    let read_dir = match fs::read_dir(path) {
        | Ok(a) => a,
        | Err(e) => {
            eprintln!("{}Invalid XEPKG setup: Unable to read {}{}{}: {}.{}",
                      color,
                      color_reset,
                      path.display(),
                      color,
                      e,
                      color_reset);
            exit(1);
        }
    };
    for entry in read_dir {
        let entry = match entry {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}Invalid XEPKG setup: Unable to read an entry in {}{}{}: {}.{}", color, color_reset, path.display(), color, e, color_reset);
                exit(1);
            }
        };
        let file_name = match entry.file_name().into_string() {
            | Ok(a) => a,
            | Err(_) => {
                eprintln!("{}Invalid XEPKG setup: Unable to read file name in {}/etc/xepkg/repo/{}: Invalid UTF8.{}", color, color_reset, color, color_reset);
                exit(1);
            }
        };
        if file_name.ends_with(".bundle") {
            list.push(file_name.trim_end_matches(".bundle").to_string());
        }
    }
    list
}

impl Repo {
    pub fn try_update(self, color: &str, color_reset: &str) {
        // It contains the type of archive in the second item and the base url in the first.
        let repo_link: Vec<&str> = self.link.split("/LATEST").collect();

        let web_version =
            match reqwest::blocking::get(&format!("{}/LATEST.version",
                                                  repo_link[0]))
            {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to retrieve file from {}{}/version{}: {}.{}", color, color_reset, repo_link[0], color, e, color_reset);
                    exit(1);
                }
            };
        let web_version = match web_version.text() {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: Unable to get text from file: {}{}/version{}: {}.{}", color, color_reset, repo_link[0], color, e, color_reset);
                exit(1);
            }
        };
        let web_version_vec: Vec<&str> = web_version.split('.').collect();
        let mut web_version = Vec::new();
        for i in web_version_vec {
            let num = i.parse::<u8>().unwrap_or(0);
            web_version.push(num);
        }

        if is_newer(&web_version, &self.version) {
            let archive = match reqwest::blocking::get(&format!("{}/LATEST{}",
                                                                repo_link[0],
                                                                repo_link[1]))
            {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to retrieve file from {}{}/LATEST{}{}: {}.{}", color, color_reset, repo_link[0], repo_link[1], color, e, color_reset);
                    exit(1);
                }
            };
            let archive = match archive.bytes() {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to get bytes from file: {}{}/LATEST{}{}: {}.{}", color, color_reset, repo_link[0], repo_link[1], color, e, color_reset);
                    exit(1);
                }
            };

            // Write the archive
            let mut file = match fs::File::create(Path::new(&format!("/etc/xepkg/repo/LATEST{}", repo_link[1]))) {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to create {}/etc/xepkg/repo/LATEST{}{}: {}.{}", color, color_reset, repo_link[1], color, e, color_reset);
                    exit(1);
                }
            };

            match file.write_all(&archive) {
                | Ok(_) => (),
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to write to {}/etc/xepkg/repo/LATEST{}{}: {}.{}", color, color_reset, repo_link[1], color, e, color_reset);
                    exit(1);
                }
            }

            // Extract the archive to /etc/xepkg/repo/REPO_NAME
            let mut child = match Command::new("tar").arg("xaf").arg(format!("/etc/xepkg/repo/LATEST{}", repo_link[1])).arg("-C").arg(format!("/etc/xepkg/repo/{}", self.name)).spawn() {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to launch command `tar xaf /etc/xepkg/repo/LATEST{} -C /etc/xepkg/repo/{}`: {}.{}", color, repo_link[1], self.name, e, color_reset);
                    exit(1);
                }
            };
            // Assume it worked(for now)
            match child.wait() {
                | Ok(_) => (),
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to wait for `tar` to finish: {}.{}", color, e, color_reset);
                    exit(1);
                }
            };
        }
    }

    pub fn new(url: &str, color: &str, color_reset: &str) -> Repo {
        // Remove /etc/xepkg/repo/tmp if it exists
        if Path::new("/etc/xepkg/repo/tmp").exists() {
            match fs::remove_dir_all("/etc/xepkg/repo/tmp") {
                | Ok(_) => (),
                | Err(e) => {
                    eprintln!("{}ERROR: unable to remove {}/etc/xepkg/repo/tmp{}: {}.{}", color, color_reset, color, e, color_reset);
                    exit(1);
                }
            }
        }

        // Get the extension of the desired file
        let mut archive_ending: Vec<&str> = url.split('.').collect();
        archive_ending.remove(0);

        let archive_ending = archive_ending.join(".");

        let archive = match reqwest::blocking::get(url) {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: Unable to retrieve file from {}{}{}: {}.{}", color, color_reset, url, color, e, color_reset);
                exit(1);
            }
        };
        let archive = match archive.bytes() {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: Unable to get bytes from file: {}{}{}: {}.{}", color, color_reset, url, color, e, color_reset);
                exit(1);
            }
        };

        // Write the archive
        let mut file = match fs::File::create(Path::new(&format!("/etc/xepkg/repo/LATEST{}", archive_ending))) {
            | Ok(a) => a,
            | Err(e) => {
                eprintln!("{}ERROR: Unable to create {}/etc/xepkg/repo/LATEST{}{}: {}.{}", color, color_reset, archive_ending, color, e, color_reset);
                exit(1);
            }
        };

        match file.write_all(&archive) {
            | Ok(_) => (),
            | Err(e) => {
                eprintln!("{}ERROR: Unable to write to {}/etc/xepkg/repo/LATEST{}{}: {}.{}", color, color_reset, archive_ending, color, e, color_reset);
                exit(1);
            }
        }

        // Extract the archive to /etc/xepkg/repo/tmp
        let mut child =
            match Command::new("tar").arg("xaf")
                                     .arg(format!("/etc/xepkg/repo/LATEST{}",
                                                  archive_ending))
                                     .arg("-C")
                                     .arg("/etc/xepkg/repo/tmp")
                                     .spawn()
            {
                | Ok(a) => a,
                | Err(e) => {
                    eprintln!("{}ERROR: Unable to launch command `tar xaf /etc/xepkg/repo/LATEST{} -C /etc/xepkg/repo/tmp`: {}.{}", color, archive_ending, e, color_reset);
                    exit(1);
                }
            };

        // Assume it worked(for now)
        match child.wait() {
            | Ok(_) => (),
            | Err(e) => {
                eprintln!("{}ERROR: Unable to wait for `tar` to finish: {}.{}",
                          color, e, color_reset);
                exit(1);
            }
        };

        // Grab the name
        let repo =
            parser::parse_repo("/etc/xepkg/repo/tmp/info", color, color_reset);
        // Rename /etc/xepkg/repo/tmp to /etc/xepkg/repo/REPO_NAME
        match fs::rename("/etc/xepkg/repo/tmp",
                         format!("/etc/xepkg/repo/{}", repo.name))
        {
            | Ok(_) => (),
            | Err(e) => {
                eprintln!("{}ERROR: unable to rename {}/etc/xepkg/repo/tmp{} to {}/etc/xepkg/repo/{}{}: {}.{}", color, color_reset, color, color_reset, repo.name, color, e, color_reset);
                exit(1);
            }
        }
        repo
    }
}
