// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
use std::{fmt, fs, path::Path, process::exit};

use crate::vec_str_to_vec_string;

pub fn parse(path: &str, color: &str, color_reset: &str) -> Xepkg {
    let file = match fs::read_to_string(Path::new(&path)) {
        | Ok(a) => a,
        | Err(_) => {
            eprintln!("Could not read file {}.", &path);
            exit(1);
        }
    };

    let file_lines: Vec<&str> = file.split('\n').collect();
    let mut file_lines = vec_str_to_vec_string(file_lines);

    // Initialize the struct variables.
    let mut repository = String::new();
    let mut version = String::new();
    let mut description = String::new();
    let mut license = String::new();
    let mut status = String::new();
    let mut keywords = Vec::new();
    let mut deps = Vec::new();
    let mut optional_deps = Vec::new();
    let mut optional_features = Vec::new();
    let mut build_cmd = String::new();
    let mut rm_cmd = String::new();
    let name = match file_lines[0].starts_with('[')
                     && file_lines[0].ends_with(']')
    {
        | true => file_lines[0].trim_start_matches('[')
                               .trim_end_matches(']')
                               .to_string(),
        | false => {
            eprintln!("{}Invalid XEPKG file format: line 1: format [NAME].{}",
                      color, color_reset);
            exit(1);
        }
    };

    file_lines.remove(0);

    let mut counter = 0;
    for i in file_lines {
        counter += 1;
        if i.starts_with("repo: ") {
            repository = i.trim_start_matches("repo: ").to_string();
        }
        if i.starts_with("version: ") {
            version = i.trim_start_matches("version: ").to_string();
        }
        if i.starts_with("description: ") {
            description = i.trim_start_matches("description: ").to_string();
        }
        if i.starts_with("license: ") {
            license = i.trim_start_matches("license: ").to_string();
        }
        if i.starts_with("status: ") {
            status = match &i.trim_start_matches("status: ")
                             .to_ascii_uppercase()[..]
            {
                | "STABLE" => "Stable".to_string(),
                | "BROKEN" => "Broken".to_string(),
                | "UNKNOWN" => "Unknown".to_string(),
                | _ => {
                    eprintln!("{}Invalid XEPKG file format: line {}: format: status: (STABLE|BROKEN|UNKNOWN).{}", color, counter, color_reset);
                    exit(1);
                }
            }
        }
        if i.starts_with("keywords: ") {
            let mut keywords_string =
                i.trim_start_matches("keywords: ").to_string();
            keywords_string = keywords_string.trim_start_matches('[')
                                             .trim_end_matches(']')
                                             .to_string();
            let temp_vec: Vec<&str> = keywords_string.split(',').collect();
            let mut temp_vec = vec_str_to_vec_string(temp_vec);
            for j in temp_vec.iter_mut() {
                *j = j.trim_start_matches(' ').to_string();
            }
            keywords = temp_vec;
        }
        if i.starts_with("deps: ") {
            let mut deps_string = i.trim_start_matches("deps: ").to_string();
            deps_string = deps_string.trim_start_matches('[')
                                     .trim_end_matches(']')
                                     .to_string();
            let temp_vec: Vec<&str> = deps_string.split(',').collect();
            let mut temp_vec = vec_str_to_vec_string(temp_vec);
            for j in temp_vec.iter_mut() {
                *j = j.trim_start_matches(' ').to_string();
            }
            deps = temp_vec;
        }
        if i.starts_with("optional-deps: ") {
            let mut optional_deps_string =
                i.trim_start_matches("optional-deps: ").to_string();
            optional_deps_string = optional_deps_string.trim_start_matches('[')
                                                       .trim_end_matches(']')
                                                       .to_string();
            let temp_vec: Vec<&str> =
                optional_deps_string.split("),").collect();
            let mut temp_vec = vec_str_to_vec_string(temp_vec);
            for j in temp_vec.iter_mut() {
                *j = j.trim_start_matches(' ')
                      .trim_start_matches('(')
                      .trim_end_matches(')')
                      .to_string();
            }
            let mut tuple_vec = Vec::new();
            for j in temp_vec {
                let vec: Vec<&str> = j.split(',').collect();
                if !vec.is_empty() {
                    if vec.len() != 2 {
                        if !&i.contains('(') && !&i.contains(',') {
                            break;
                        } else {
                            eprintln!("{}Invalid XEPKG file format: line {}: format: optional-deps: [(DEP_NAME, --enable-dep-flag)].{}", color, counter, color_reset);
                            exit(1);
                        }
                    } else {
                        tuple_vec.push((vec[0].to_string(),
                                        vec[1].to_string()));
                    }
                }
            }
            optional_deps = tuple_vec;
        }
        if i.starts_with("optional-feat: ") {
            let mut optional_feat_string =
                i.trim_start_matches("optional-feat: ").to_string();
            optional_feat_string = optional_feat_string.trim_start_matches('[')
                                                       .trim_end_matches(']')
                                                       .to_string();
            let temp_vec: Vec<&str> =
                optional_feat_string.split("),").collect();
            let mut temp_vec = vec_str_to_vec_string(temp_vec);
            for j in temp_vec.iter_mut() {
                *j = j.trim_start_matches(' ')
                      .trim_start_matches('(')
                      .trim_end_matches(')')
                      .to_string();
            }
            let mut tuple_vec = Vec::new();
            for j in temp_vec {
                let vec: Vec<&str> = j.split(',').collect();
                if !vec.is_empty() {
                    if vec.len() != 2 {
                        if !&i.contains('(') && !&i.contains(',') {
                            break;
                        } else {
                            eprintln!("{}Invalid XEPKG file format: line {}: format: optional-feat: [(FEATURE_NAME, --enable-feature-flag)].{}", color, counter, color_reset);
                            exit(1);
                        }
                    } else {
                        tuple_vec.push((vec[0].to_string(),
                                        vec[1].to_string()));
                    }
                }
            }
            optional_features = tuple_vec;
        }
        if i.starts_with("build: ") {
            build_cmd = i.trim_start_matches("build: ").to_string();
        }
        if i.starts_with("rm: ") {
            rm_cmd = i.trim_start_matches("rm: ").to_string();
        }
    }
    for i in 0 .. keywords.len() {
        if keywords[i].is_empty() {
            keywords.remove(i);
        }
    }
    for i in 0 .. deps.len() {
        if deps[i].is_empty() {
            deps.remove(i);
        }
    }

    Xepkg { name,
            repository,
            version,
            description,
            license,
            status,
            keywords,
            deps,
            optional_deps,
            optional_features,
            build_cmd,
            rm_cmd }
}

#[derive(PartialEq, Clone, Debug)]
pub struct Xepkg {
    pub name:              String,
    pub repository:        String,
    pub version:           String,
    pub description:       String,
    pub license:           String,
    pub status:            String,
    pub keywords:          Vec<String>,
    pub deps:              Vec<String>,
    pub optional_deps:     Vec<(String, String)>,
    pub optional_features: Vec<(String, String)>,
    pub build_cmd:         String,
    pub rm_cmd:            String
}

// For debugging
impl fmt::Display for Xepkg {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(
               formatter,
               "Xepkg:
\tName: {}
\tRepo: {}
\tVers: {}
\tDesc: {}
\tLice: {}
\tStat: {}
\tKeys: {:?}
\tDeps: {:?}
\tO-De: {:?}
\tO-Fe: {:?}
\tBuil: {}
\tRemo: {}",
               self.name,
               self.repository,
               self.version,
               self.description,
               self.license,
               self.status,
               self.keywords,
               self.deps,
               self.optional_deps,
               self.optional_features,
               self.build_cmd,
               self.rm_cmd
        )
    }
}

// A valid file looks like:
// [main]
// version: 1.2.0
// link: https://rsterrier.dev/xerux/main/LATEST.tar.xz
//
// The link `https://rsterrier.dev/xerux/main/LATEST.version` must also exist
pub struct Repo {
    pub name:    String,
    pub link:    String,
    pub version: Vec<u8>
}

pub fn parse_repo(path: &str, color: &str, color_reset: &str) -> Repo {
    let mut output = Repo { name:    String::new(),
                            link:    String::new(),
                            version: Vec::new() };

    let file = match fs::read_to_string(Path::new(&path)) {
        | Ok(a) => a,
        | Err(_) => {
            eprintln!("{}Could not read file {}.{}", color, &path, color_reset);
            exit(1);
        }
    };

    let file_lines: Vec<&str> = file.split('\n').collect();
    let file_lines = vec_str_to_vec_string(file_lines);

    if file_lines[0].starts_with('[') && file_lines[0].ends_with(']') {
        output.name = file_lines[0].trim_start_matches('[')
                                   .trim_end_matches(']')
                                   .to_string();
        if format!("/etc/xepkg/repo/{}", output.name)
           != path.trim_end_matches('/').trim_end_matches("/info")
        {
            eprintln!("{}Invalid REPO file format: line 1: [REPO-NAME] must match path: found {}/etc/xepkg/repo/{}{}, expected {}{}{}.{}",
                      color,
                      color_reset,
                      output.name,
                      color,
                      color_reset,
                      path.trim_end_matches('/').trim_end_matches("/info"),
                      color,
                      color_reset);
            exit(1);
        }
    } else {
        eprintln!("{}Invalid REPO file format: line 1: format [REPO-NAME].{}",
                  color, color_reset);
        exit(1);
    }

    for (i, j) in file_lines.iter().enumerate() {
        if i == 0 && j.starts_with('[') && j.ends_with(']') {
            output.name =
                j.trim_start_matches('[').trim_end_matches(']').to_string();
            if format!("/etc/xepkg/repo/{}", output.name)
               != path.trim_end_matches('/').trim_end_matches("/info")
            {
                eprintln!("{}Invalid REPO file format: line 1: [REPO-NAME] must match path: found {}/etc/xepkg/repo/{}{}, expected {}{}{}.{}",
                          color,
                          color_reset,
                          output.name,
                          color,
                          color_reset,
                          path.trim_end_matches('/').trim_end_matches("/info"),
                          color,
                          color_reset);
                exit(1);
            }
        } else if i == 0 {
            eprintln!("{}Invalid REPO file format: line 1: format [REPO-NAME].{}", color, color_reset);
            exit(1);
        } else if j.starts_with("link: ") {
            output.link = j.trim_start_matches("link: ").to_string();
        } else if j.starts_with("version: ") {
            let version =
                j.trim_start_matches("version: ").replace('_', "").to_string();
            // Split it into a vector
            let version_vector_string: Vec<&str> = version.split('.').collect();
            output.version = Vec::new();
            for i in version_vector_string {
                let num = i.parse::<u8>().unwrap_or(0);
                output.version.push(num);
            }
        }
    }

    output
}
