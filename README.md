# Xepkg
Xepkg is a source based package manager which is primarily focused on user choice.

## Movivation
For starters, most source based package managers, such as kiss, portage, and sorcery, are written in interpreted languages, which removes having computer specific optimizations to your programs, because you never compile them. Another problem I noticed was the package manager making decisions on which flags to enable or disable, which removes one of the biggest reasons for using a source based package manager, which is the choice to enable and disable any features you don't need to have full control over everything.

## Building
Before building, you must have openssl, libressl, or a comparable ssl library, and rustlang installed. The nightly branch is required(for rustfmt).
To build run `cargo build --release` and then copy the executable(located in `target/release/xepkg`) to somewhere in the $PATH(use `/usr/local/bin` if you're unsure what this means).

To use after doing this, create `/etc/xepkg/repo` and `/var/cache/xepkg` as root.

To install the man-page copy `xepkg.1` to `/usr/share/man/man1/`.

## Usage
Run `xepkg --help` to see a list of commands or read the man page.
